<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_infos', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('surname');
            //length of south african id number
            $table->decimal('south_african_id',13, 0)->unique();
            //length of south african phone number plus dialing code
            $table->string('mobile');
            $table->string('email')->unique();
            $table->date('date_of_birth');
            $table->foreignId('language_options_id')->constrained();
            $table->json('interests');
            $table->timestamps();
        });
        $data = [
            [
                'name' => 'Zane',
                'surname' => 'Smith',
                'south_african_id' => '1231231231231',
                'mobile' => '0123456789',
                'email' => 'zane@email.com',
                'date_of_birth' => '1972-01-21',
                'language_options_id' => '1',
                'interests' => '{"Interests":["Golf","Hiking"]}'
            ],
            [
                'name' => 'John',
                'surname' => 'Nel',
                'south_african_id' => '4564564564564',
                'mobile' => '0123456789',
                'email' => 'john@email.com',
                'date_of_birth' => '1996-07-13',
                'language_options_id' => '2',
                'interests' => '{"Interests":["Golf","Hiking"]}'
            ],
            [
                'name' => 'Natalie',
                'surname' => 'Jackson',
                'south_african_id' => '7897897897897',
                'mobile' => '0123456789',
                'email' => 'Natalie@email.com',
                'date_of_birth' => '1989-05-18',
                'language_options_id' => '3',
                'interests' => '{"Interests":["Golf","Hiking"]}'
            ]
        ];
        DB::table('client_infos')->insert($data);


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_infos');
    }
}
