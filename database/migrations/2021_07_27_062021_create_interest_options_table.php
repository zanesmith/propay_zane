<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInterestOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interest_options', function (Blueprint $table) {
            $table->id();
            $table->string('interests');
            $table->timestamps();
        });
        $data =[
            ['interests' => 'Golf'],
            ['interests' => 'Tennis',],
            ['interests' => 'Hiking']
        ];
        DB::table('interest_options')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interest_options');
    }
}
