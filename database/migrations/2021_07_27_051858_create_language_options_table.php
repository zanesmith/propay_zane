<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLanguageOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('language_options', function (Blueprint $table) {
            $table->id();
            $table->string('language');
            $table->timestamps();
        });
        $data =[
            ['language' => 'English'],
            ['language' => 'Arabic',],
            ['language' => 'Latin']
        ];
        DB::table('language_options')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('language_options');
    }
}
