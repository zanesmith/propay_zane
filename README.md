<p align="center"><img src="https://www.propay.co.za/img/Propay-Logo.png" width="400"></a></p>

## Zane ProPay Test

This Assignment was created by Zane Smith to demonstrate skills for a senior development position

##important notes to consider

user table was created and used as an "admin Login" so that I could use laravel's authentication pages and functionality 
for login, register and logout. however because I used this system to gain access to the assessment "admin crud page" I 
manipulated said functionality to only allow registration of users once an admin has already logged in. I also built a 
command to register an admin user.

##Setup and Configuration

1 - Clone the repo to your local machine:

    -git@gitlab.com:zanesmith/propay_zane.git
    
2 - Install composer dependencies from a terminal in the root project:
    
    -composer install

3 - Install node dependencies with either npm from a terminal in the root project:

    -npm install
4 - Create a local .env file:
    
    -cp .env.example .env
    -copy .env.example .env (on windows)
    
5 - update env  file
    
    -open the .env file
    -edit the following env variables to match those of your local system. Db_Database can be named whatever you want.
                DB_CONNECTION
                DB_HOST
                DB_PORT
                DB_DATABASE
                DB_USERNAME
                DB_PASSWORD
    -edit the following env variables to match your mailer scecifications, (you can 
    leave the details in there and use them if you like as I created this gmail account 
    specifically for this test and they are working)
                MAIL_MAILER
                MAIL_HOST
                MAIL_PORT
                MAIL_USERNAME
                MAIL_PASSWORD
                MAIL_ENCRYPTION
                MAIL_FROM_ADDRESS
                MAIL_FROM_NAME
      
6 - Set the application key:

    -php artisan key:generate
    
7 -Compile assets:

    -npm run dev
          
8 - create database
    
    - create a database that matches the name you assigned in the env DB_DATABASE
    
9 - run the migration to create and populate the database in a terminal navigate to the root project and run:

    -php artisan migrate
    
    *included in the migrations are some dummy data to populate the data base 

10 - *optional run command in a terminal in the root project to create a new admin user :
    
    -php artisan user:create
    
    *I wanted to demonstrate the use of comands to register a new user, however should you not want to do this you 
    can always use the default user I added into the migration:
    -name: admin
    -email: admin@admin.com
    -password: admin
    
11 - serve the app in a terminal in the root project

    -php artisan serve
    
##Using the app

1 - navigate to 
    
    ("/customer-details") or ("/") will also redirect you accordingly
    
2 - when prompted to login, use details set in user:create command
    
    alternatively you can use preloaded details:
    -name: admin
    -email: admin@admin.com
    -password: admin
    
3 - on the left hand navigation, there are buttons that do the following:
    
    - register new admin -> allows you to register a new admin using laravels built in auth function
    - Logout admin -> logs you out of the panel
    - add new language -> creates a new language option for customer information
    - add new interest -> creates a new interest option for customer information
    - add new Customer -> allows you to add a new customer 
    
4 - on the right hand of the navigation,

    - clients information can be swaped by electing each client name
    - information can be updated by selecting the edit tab 
    - clients can be deleted using the delete button
    
5 - on creation of client and event listener triggers an email to be fired off to the new customer