<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class clientInfo extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','name', 'surname', 'south_african_id', 'mobile', 'email', 'date_of_birth', 'language_options_id', 'interests'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
//        'interests' => 'Array',
    ];


    public function languageOptions()
    {
        return $this->belongsTo('App\languageOptions');
    }
}
