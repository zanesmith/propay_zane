<?php

namespace App\Listeners;

use App\Events\CustomerAdded;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\ClientAdded;
use Illuminate\Support\Facades\Mail;

class SendCustomerAddedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CustomerAdded  $event
     * @return void
     */
    public function handle(CustomerAdded $event)
    {
        $client = $event->client;
        return Mail::to($client->email)->send(new ClientAdded($client));
    }
}
