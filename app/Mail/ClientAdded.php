<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\clientInfo;

class ClientAdded extends Mailable
{
    use Queueable, SerializesModels;

    public $client;
    /**
     * Create a new message instance.
     *
     * @param  \App\clientInfo  $client
     * @return void
     */
    public function __construct(clientInfo $client)
    {
        $this->client = $client;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.welcome')->subject('Welcome to ProPay')->from('zane.propay@gmail.com');
    }
}
