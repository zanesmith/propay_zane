<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\CustomerAdded;
use App\clientInfo;
use App\LanguageOptions;
use App\InterestOptions;

class CustomerDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = clientInfo::all();
        $languages = LanguageOptions::all();
        $interests = InterestOptions::all();
        return view('admin/customerDetails', ['clients' => $clients, 'languages' => $languages, 'interests' => $interests]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function languageStore(Request $request)
    {
        $language = $request->all();

        $request->validate([
            'language' => 'required|max:191',
        ]);
        LanguageOptions::create($language);

        return back()
            ->with('success', $language['language'] .' Language has been added to options Successfully');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function InterestStore(Request $request)
    {
        $interest = $request->all();

        $request->validate([
            'interests' => 'required|max:191',
        ]);
        InterestOptions::create($interest);

        return back()
            ->with('success', $interest['interests'] .' has been added to the interest options Successfully');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = $request->all();
        $request->validate([
            'name' => 'required|String|max:191',
            'surname' => 'required|String|max:191',
            'south_african_id' => 'required|unique:client_infos|digits:13',
            'mobile' => 'required|digits_between:10,13',
            'email' => 'required|email|unique:client_infos|max:191',
            'date_of_birth' => 'required|date|before:today',
            'language_options_id' => 'required|exists:Language_Options,id',
            'interests .*' => 'required|exists:InterestOptions,interests'
        ]);
        $customer['interests'] = [ 'Interests' => $customer['interests']];
        $customer['interests'] = json_encode($customer['interests']);
        $addedCustomer = clientInfo::create($customer);
        event(new CustomerAdded($addedCustomer));
        return back()
            ->with('success', $customer['name'] .' ' . $customer['surname'] .' has been added to the interest options Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param   int  $clientId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,int $clientId)
    {
        $client = clientInfo::find($clientId);
        $customer = $request->all();
        $request->validate([
            'name' => 'required|String|max:191',
            'surname' => 'required|String|max:191',
            'south_african_id' => 'required|digits:13|unique:client_infos,south_african_id,'.$clientId,
            'mobile' => 'required|digits_between:10,13',
            'email' => 'required|email|max:191|unique:client_infos,email,'.$clientId,
            'date_of_birth' => 'required|date|before:today',
            'language_options_id' => 'required|exists:Language_Options,id',
            'interests .*' => 'required|exists:InterestOptions,interests'
        ]);
        $customer['interests'] = [ 'Interests' => $customer['interests']];
        $customer['interests'] = json_encode($customer['interests']);
        $client->update($customer);
        return back()
            ->with('success', $customer['name'] .' ' . $customer['surname'] .' has been updated t');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = clientInfo::find($id);
        $name = $client->name . ' ' . $client->surname;
        $client->delete();
        return back()
            ->with('success', $name .' has been deleted Successfully');
    }
}
