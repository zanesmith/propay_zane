<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use Hash;


class UserCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Created by Zane Smith to illustrating using commands and to create a user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $user['name'] = $this->ask('What is your name?');
        $user['email'] = $this->ask('What is your email?');
        $user['password'] = Hash::make($this->secret('Please enter a password.'));
        user::create($user);
        $this->info('Thank you ' . $user['name'] . ' user registered');

    }
}
