<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div class="container ">
    <section class="section-content py-3 ">
        <div class="row h-100">
            <aside class="col-lg-3 ">
                <!-- ============= COMPONENT ============== -->
                <nav class="sidebar card py-2 h-100">
                    <ul class="nav flex-column">
                        <li class="nav-item p-2">
                            <a class="nav-link" href="#">
                                <img class="logo" src="https://www.propay.co.za/img/Propay-Logo.png" style="max-width: 10rem">
                            </a>
                        </li>

                        @yield('sidebar')
                    </ul>
                </nav>
                <!-- ============= COMPONENT END// ============== -->
            </aside>
            <main class="col-lg-9 bg-white" >
                @yield('content')

            </main>
        </div>
    </section>
</div>
</body>
</html>