<!-- Modal -->
<div class="modal fade" id="{{$id}}" tabindex="-1" aria-labelledby="{{$labelledby}}" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="card">
                <div class=" modal-header card-header">
                    @yield('header')

                </div>
                <div class=" modal-body card-body">
                    @yield('body')
                </div>
            </div>
        </div>
    </div>
</div>