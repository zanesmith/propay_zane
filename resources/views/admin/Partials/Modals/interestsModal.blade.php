@extends('..layouts.modalLayout', ['id' => 'interestModal', 'labelledby' => 'interestModalLabel'])
@section('header')
    <h5>Add a Interest for client dropdown</h5>
@overwrite
@section('body')
    <form action="{{ route('CustomerDetailsController.InterestStore') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label><strong>Interest : </strong></label>
            <input type="text" name="interests" class="form-control">
        </div>
        <div class="form-group text-center">
            <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Close</button>
            <input type="submit" class="btn btn-outline-success" name="submit" value="Save">
        </div>
    </form>
@overwrite
