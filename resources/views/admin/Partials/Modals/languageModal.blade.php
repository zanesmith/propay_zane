@extends('..layouts.modalLayout', ['id' => 'languageModal', 'labelledby' => 'languageModalLabel'])
@section('header')
    <h5>Add a language for client dropdown</h5>
@overwrite
@section('body')
    <form action="{{ route('CustomerDetailsController.languageStore') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label><strong>Language : </strong></label>
            <input type="text" name="language" class="form-control">
        </div>
        <div class="form-group text-center">
            <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Close</button>
            <input type="submit" class="btn btn-outline-success" name="submit" value="Save">
        </div>
    </form>
@overwrite
