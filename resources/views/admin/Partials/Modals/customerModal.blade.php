@extends('..layouts.modalLayout', ['id' => 'customerModal', 'labelledby' => 'customerModalLabel'])
@section('header')
    <h5>Add a new Customer</h5>
@overwrite
@section('body')
    <form action="{{ route('customer-details.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label><strong>Name : </strong></label>
            <input type="text" name="name" class="form-control">
        </div>
        <div class="form-group">
            <label><strong>Surname : </strong></label>
            <input type="text" name="surname" class="form-control">
        </div>
        <div class="form-group">
            <label><strong>South African Id No : </strong></label>
            <input type="number" name="south_african_id" class="form-control">
        </div>
        <div class="form-group">
            <label><strong>Mobile No : </strong></label>
            <input type="number" name="mobile" class="form-control">
        </div>
        <div class="form-group">
            <label><strong>Email : </strong></label>
            <input type="email" name="email" class="form-control">
        </div>
        <div class="form-group">
            <label><strong>Date Of Birth : </strong></label>
            <input type="date" name="date_of_birth" class="form-control">
        </div>
        <div class="form-group">
            <label><strong>Language : </strong></label>
            <select class="form-select form-control" name="language_options_id" aria-label="Select One Language">
                <option selected value="">Select One Language</option>
                @foreach($languages as $language)
                    <option value="{{$language->id}}">{{$language->language}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label><strong>Interests : </strong> (hold ctrl to select multiple)</label>
            <select class="form-control" multiple="multiple" id="interests[]" name="interests[]" aria-label="Select Multiple Interests">
                @foreach($interests as $interest)
                    <option value="{{$interest->interests}}">{{$interest->interests}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group text-center">
            <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Close</button>
            <input type="submit" class="btn btn-outline-success" name="submit" value="Save">
        </div>
    </form>
@overwrite
