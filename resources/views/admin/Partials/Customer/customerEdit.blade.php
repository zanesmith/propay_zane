<div class="tab-pane fade" id="nav-edit-{{$client->id}}" role="tabpanel" aria-labelledby="nav-edit-{{$client->id}}-tab">
    <h3 class="display-5">Edit Details for: {{$client->name}} {{$client->surname}}</h3>
    <blockquote class="blockquote">
        <form action="{{ route('customer-details.update', $client) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <p>Name: <strong><s>{{$client->name}}</s> <input type="text" name="name" value="{{$client->name}}" class="form-control"></strong></p>
            <p>Surname: <strong><s>{{$client->surname}}</s> <input type="text" name="surname" value="{{$client->surname}}" class="form-control"></strong></p>
            <p>South African Id: <strong><s>{{$client->south_african_id}}</s> <input type="number" name="south_african_id" value="{{$client->south_african_id}}" class="form-control"></strong></p>
            <p>Mobile Number: <strong><s>{{$client->mobile}}</s> <input type="number" name="mobile" value="{{$client->mobile}}" class="form-control"></strong></p>
            <p>Email Address: <strong><s>{{$client->email}}</s> <input type="email" name="email" value="{{$client->email}}" class="form-control"></strong></p>
            <p>Birth Date: <strong><s>{{$client->date_of_birth}}</s> <input type="date" name="date_of_birth" value="{{$client->date_of_birth}}" class="form-control"></strong></p>
            <p>Language: <strong><s>{{$client->languageOptions->language}}</s> <select class="form-select form-control" name="language_options_id" aria-label="Select One Language">
                        @foreach($languages as $language)
                            <option @if($language->id == $client->language_options_id) selected @endif value="{{$language->id}}">{{$language->language}}</option>
                        @endforeach
                    </select></strong></p>
            <p>Interests: <strong><s>
                        @foreach(json_decode($clients[0]->interests)->Interests as $i)
                            {{$i}},
                        @endforeach</s>
                    <select class="form-control" multiple="multiple" id="interests[]" name="interests[]" aria-label="Select Multiple Interests">
                        @foreach($interests as $interest)
                            <option @if( in_array($interest->interests, json_decode($clients[0]->interests)->Interests)) selected @endif value="{{$interest->interests}}">{{$interest->interests}}</option>
                        @endforeach
                    </select></strong></p>

            <div class="form-group text-center">
                <input type="submit" class="btn btn-outline-success" name="submit" value="Update">
            </div>
        </form>
    </blockquote>
</div>