<div class="tab-pane fade show active" id="nav-display-{{$client->id}}" role="tabpanel" aria-labelledby="nav-display-{{$client->id}}-tab">
    <h3 class="display-5">{{$client->name}} {{$client->surname}}</h3>
    <blockquote class="blockquote">
        <p>South African Id: <strong>{{$client->south_african_id}}</strong></p>
        <p>Mobile Number: <strong>{{$client->mobile}}</strong></p>
        <p>Email Address: <strong>{{$client->email}}</strong></p>
        <p>Birth Date: <strong>{{$client->date_of_birth}}</strong></p>
        <p>Language: <strong>{{$client->languageOptions->language}}</strong></p>
        <p>Interests: <strong>
                @foreach(json_decode($clients[0]->interests)->Interests as $interest)
                    {{$interest}},
                @endforeach
            </strong></p>
    </blockquote>
    <button href="" onclick="if(confirm('Do you want to delete this {{$client->name}}?'))event.preventDefault(); document.getElementById('delete-{{$client->id}}').submit();" class="btn btn-outline-danger ">Delete</button>
    <form id="delete-{{$client->id}}" method="post" action="{{route('customer-details.destroy',$client)}}" style="display: none;">
        @csrf
        @method('DELETE')
    </form>
</div>