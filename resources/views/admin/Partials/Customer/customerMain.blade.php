<div class="d-flex align-items-start">
    <div class="nav flex-column nav-tabs me-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">
        @foreach ($clients as $client)
            <button class="nav-link  @if ($loop->first) active @endif " id="v-pills-{{$client->id}}-tab" data-bs-toggle="pill" data-bs-target="#v-pills-{{$client->id}}" type="button" role="tab" aria-controls="v-pills-{{$client->id}}" aria-selected="false">{{$client->name.' '.$client->surname}}</button>
        @endforeach
    </div>
    <div class="tab-content" id="v-pills-tabContent">
        @foreach ($clients as $client)
            <div class="tab-pane fade @if ($loop->first) show active @endif" id="v-pills-{{$client->id}}" role="tabpanel" aria-labelledby="v-pills-{{$client->id}}-tab">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <button class="nav-link active" id="nav-display-{{$client->id}}-tab" data-bs-toggle="tab" data-bs-target="#nav-display-{{$client->id}}" type="button" role="tab" aria-controls="nav-display-{{$client->id}}" aria-selected="true">Display</button>
                        <button class="nav-link" id="nav-edit-{{$client->id}}-tab" data-bs-toggle="tab" data-bs-target="#nav-edit-{{$client->id}}" type="button" role="tab" aria-controls="nav-edit-{{$client->id}}" aria-selected="false">Edit</button>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    @include('admin.Partials.Customer.customerDisplay',['client' => $client])
                    @include('admin.Partials.Customer.customerEdit',['client' => $client])
                </div>
            </div>
        @endforeach
    </div>
</div>